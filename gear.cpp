/*
the gear curve is a curve resembling a gear with n teeth given by the parametric equations
x	=	r*cos*t	
y	=	r*sin*t,	

where
r=a+1/b*tanh[b*sin(n*t)], 	
The trig functions take radians, not degrees.
where tanhx is the hyperbolic tangent. Plots above show gear curves for a=1, b=10, and n=1 to 12. */
#include <iostream>
#include <fstream>
#include <math.h>
#include "/usr/include/gnuplot-iostream.h"

using namespace std;
int main(){
		Gnuplot gp;

	ofstream plot;
	plot.open("gear.dat");
	double pi = 3.14159265359;
	double r = 1;
	int t = 0; //theta, might need to be an itterator.
	double a = 1.3;
	double b = 6;
	double xx = r*cos(t);
	double yy = r*sin(t);

    double rad = 0;
	for(t=0;t <= 360; t+=1){
    rad =(t*pi)/180;
	r = a+(1/b)*tanh(b*sin(12*rad));
    xx = r*cos(rad);
	yy = r*sin(rad);
//cout << r << " " << xx << " " << yy << " " <<  endl;
plot << r << " " << xx << " " << yy << " " <<  endl;
}
	plot.close();
        gp <<"set terminal qt 0\n";	
		gp << "plot \"gear.dat\" using 2:3 with lines\n";
gp <<"set terminal qt 1\n";
gp<<"set polar\n";
		gp << "plot \"gear.dat\" using 1:3 with lines\n";
gp <<"set terminal qt 2\n";
		gp << "plot \"gear.dat\" using 2:1 with lines\n";
gp<<"unset polar\n";
gp <<"terminal qt 3\n";
		gp << "plot \"gear.dat\" using 1:2 with lines\n";
	//plot stuff
	system("");
	
	return 0;
	}
